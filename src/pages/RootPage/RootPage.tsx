import React, { FC, useState, useEffect } from "react";

import "./RootPage.scss";

import Button from "components/Button";
import { ICardProps } from "components/Card/Card";
import Container from "components/Container";
import { IColumnsProps } from "components/Container/Container";
import localStorageObj from "utils";
interface IRootPageProps {
  dataOnStorage: string | null;
}

const RootPage: FC<IRootPageProps> = ({ dataOnStorage }) => {
  const [cols, setCols] = useState<Array<IColumnsProps>>(
    dataOnStorage
      ? JSON.parse(dataOnStorage)
      : [
          { id: "1_toDo", name: "toDo", cards: [] },
          { id: "2_doing", name: "doing", cards: [] },
        ],
  );
  const storageMethods = localStorageObj.getInstance();

  // Save data to localStorage when it's being modified
  useEffect(() => {
    storageMethods.setData(cols);
  }, [cols]);

  return (
    <div className="rootpage__container">
      {cols.map((col: IColumnsProps) => (
        <Container
          name={col.name}
          key={col.id}
          id={col.id}
          defaultCards={col.cards}
          addCardsToRoot={(cards, colId) => {
            setCols(
              [...cols].map((col: IColumnsProps) => {
                if (col.id === colId) {
                  return {
                    ...col,
                    cards: cards,
                  };
                } else return col;
              }),
            );
          }}
          itemDragged={(cardId, colTargetDraggeed) => {
            let draggedCard: ICardProps | undefined;
            for (const col of cols) {
              draggedCard = col.cards.find(card => card.id === cardId);
              if (draggedCard) break; // undefined is returned on the last round, so let's break the loop when founded!
            }
            setCols(
              [...cols].map((col: IColumnsProps) => {
                if (col.name === cardId.split("_")[2]) {
                  return {
                    ...col,
                    cards: col.cards.filter(item => item.id !== cardId),
                  };
                } else if (draggedCard && col.id === colTargetDraggeed) {
                  return {
                    ...col,
                    cards: [
                      ...col.cards,
                      {
                        ...draggedCard,
                        id: `${col.cards.length + 1}_${
                          draggedCard.id.split("_")[1] // We should Change the draggedCard properties! =>
                        }_${col.name}`, // Client can the cart with the same name in prev column
                        colName: col.name,
                      },
                    ],
                  };
                } else return col;
              }),
            );
          }}
        />
      ))}
      <Button
        type="addCol"
        onAdd={text =>
          setCols([
            ...cols,
            { id: `${cols.length + 1}_${text}`, name: text, cards: [] },
          ])
        }
      />
    </div>
  );
};

export default RootPage;
