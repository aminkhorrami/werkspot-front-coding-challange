import React from "react";

import ReactDOM from "react-dom";

import "./index.scss";
import RootPage from "pages/RootPage";
import localStorageObj from "utils";

const storageMethods = localStorageObj.getInstance();

ReactDOM.render(
  <React.StrictMode>
    <RootPage dataOnStorage={storageMethods.getData()} />
  </React.StrictMode>,
  document.getElementById("root"),
);
