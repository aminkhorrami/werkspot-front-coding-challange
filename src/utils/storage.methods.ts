import { IColumnsProps } from "components/Container/Container";

import Storage from "./storage.config";
enum TrelloData {
  DATA_KEY = "data",
}

class localStorageObj extends Storage<TrelloData> {
  private static instance?: localStorageObj;

  private constructor() {
    super();
  }

  public static getInstance() {
    if (!this.instance) {
      this.instance = new localStorageObj();
    }

    return this.instance;
  }

  public getData() {
    return this.get(TrelloData.DATA_KEY);
  }

  public setData(data: Array<IColumnsProps>) {
    this.set(TrelloData.DATA_KEY, JSON.stringify(data));
  }
}

export default localStorageObj;
