import React, { DragEvent, FC, useEffect, useState } from "react";

import "./Card.scss";

interface ISetNameFunc {
  (text: string): void;
}
export interface ICardProps {
  id: string;
  defaultName: string;
  colName?: string;
  setNameFunc?: ISetNameFunc;
}

const Card: FC<ICardProps> = ({ defaultName, id, setNameFunc }) => {
  const [hover, setHover] = useState<boolean>(false);
  const setHoverMode = (mode: boolean) => setHover(mode);
  const [edit, setEdit] = useState<boolean>(false);
  const setEditMode = () => setEdit(!edit);
  const [name, setName] = useState<string>(defaultName);
  const onDragStart = (e: DragEvent) => {
    e.dataTransfer?.setData("id", id);
  };
  useEffect(() => {
    setName(defaultName);
  }, [defaultName]);
  return (
    <div
      draggable
      onDragStart={onDragStart}
      onMouseEnter={() => setHoverMode(true)}
      onMouseLeave={() => setHoverMode(false)}
      className="card__container"
    >
      {!edit && <span className="card__container__title">{name}</span>}
      {hover && (
        <div className="card__container__editButton">
          {edit ? (
            <>
              <textarea
                onChange={e => setNameFunc && setNameFunc(e.target.value)}
                value={name}
              />
              <button onClick={setEditMode}>done</button>
            </>
          ) : (
            <button onClick={setEditMode}>edit</button>
          )}
        </div>
      )}
    </div>
  );
};

export default Card;
