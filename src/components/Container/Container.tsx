import React, { FC, DragEvent, useState, useEffect } from "react";

import Button from "components/Button";
import Card, { ICardProps } from "components/Card/Card";

import "./Container.scss";

export interface IColumnsProps {
  id: string;
  name: string;
  cards: Array<ICardProps>;
}
interface IDragResolver {
  (item: string, targetContainerId: string): void;
}
interface IAddResolver {
  (cards: Array<ICardProps>, idGp: string): void;
}
interface IContainerProps {
  defaultCards: Array<ICardProps>;
  name: string;
  id: string;
  itemDragged: IDragResolver;
  addCardsToRoot: IAddResolver;
}
const Container: FC<IContainerProps> = ({
  name,
  id,
  defaultCards,
  itemDragged,
  addCardsToRoot,
}) => {
  const [cards, setCards] = useState<Array<ICardProps>>(defaultCards);
  const onDragOver = (e: DragEvent) => e.preventDefault();
  const onDrop = (e: DragEvent) => {
    const cardId = e.dataTransfer.getData("id");
    const item = cards.find((card: ICardProps) => card.id === cardId);
    if (item) return; // Prevent drop on the Same Column
    itemDragged(cardId, id);
  };

  useEffect(() => {
    setCards(defaultCards); // Modify the cards state by checking parent
  }, [defaultCards]);

  return (
    <div onDrop={onDrop} onDragOver={onDragOver} className="container">
      <div className="container__title">{name}</div>
      <hr />
      {cards.map((card: ICardProps) => (
        <Card
          setNameFunc={text => {
            const modifiedCards = [...cards].map(item => {
              if (item.id === card.id) {
                return { ...item, defaultName: text };
              }
              return item;
            });
            addCardsToRoot(modifiedCards, id);
          }}
          key={card.id}
          id={card.id}
          defaultName={card.defaultName}
        />
      ))}
      <Button
        type="addCard"
        onAdd={text => {
          const newCardsList = [
            ...cards,
            {
              id: `${cards.length + 1}_${text}_${name}`,
              defaultName: text,
              colName: name,
            },
          ];
          addCardsToRoot(newCardsList, id);
        }}
      />
    </div>
  );
};

export default Container;
