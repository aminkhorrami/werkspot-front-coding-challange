import React, { FC, useState } from "react";

import "./Button.scss";

interface IFunctionResolver {
  (text: string): void;
}

interface IButtonProps {
  type: string;
  onAdd: IFunctionResolver;
}

const Button: FC<IButtonProps> = ({ type, onAdd }) => {
  const [collapsed, setCollapsed] = useState<boolean>(false);
  const [text, setText] = useState<string>("");
  const buttonModeChange = () => setCollapsed(!collapsed);
  const submitButton = () => {
    buttonModeChange();
    onAdd(text);
  };
  const innerButtonText: string =
    type === "addCol" ? "Add a list" : "Add a card";
  const placeHolderText: string =
    type === "addCol" ? "Enter a title for list.." : "Enter a title for card..";
  if (!collapsed) {
    return (
      <button
        className={type !== "addCol" ? "button__add" : "button__add__column"}
        onClick={buttonModeChange}
      >
        {innerButtonText}
      </button>
    );
  } else {
    return (
      <div
        className={type !== "addCol" ? "button__add" : "button__add__column"}
      >
        <textarea
          placeholder={placeHolderText}
          onChange={e => setText(e.target.value)}
        />
        <button className="button__submit" onClick={() => submitButton()}>
          {type === "addCol" ? "Add a list" : "Add a card"}
        </button>
      </div>
    );
  }
};
export default Button;
