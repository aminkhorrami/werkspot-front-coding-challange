# Werkspot Front End Challange

**I try to explain the main features and the reasoning regarding implementing them.**

# Tools and Utils

As it’s mentioned no library was added. App is based on create-react-app with typeScript template. I just used node-sass as a habit. Typescript even in small projects can be a big help.
My linting rules among with husky to check the rules in different stages always help to achieve a clean code.

# Architecture

Src folder with components(Container and cards) and just a page(of course the app has just one route), among with a util folder where you can find localStorage methods. I wrote a conditional Button that can cover multiple states of apps.

# Moving Cards Between Columns

There were 2 approaches in my mind when I intended to implement the moving card part.

## 1> Using The Drag/Drop

As it seems, this approach could be the most intuitive for this feature. Regarding the time limit, it seems it’s kinda tricky however I had
experience implementing this feature before(working with Drag, onDragSaart, etc. events without any specific library). So, that was like the best fit for here.

## 2> Using The DropDown Button On Each Card

The second solution which popped into my mind was putting a dropDown on each card, which user could select which column the card should be popped into. I assume the UX and functionality in this approach could be acceptable either.

# If I had more time,…

## Let’s say if I had more time to add more features or changes to the app:

1. Refactoring the code! The time was kinda short in order to write the best code. I tried to implement all features with the best architecture which popped into my mind. Of course, with more time I could think about a more performant way.
2. Of course some CSS enhancements :)
3. Some cancel buttons could be added to give more flexibility to the user.
4. Putting strings on the consts folder which I didn’t find time to achieve.

## Looking forward to hearing from you!
